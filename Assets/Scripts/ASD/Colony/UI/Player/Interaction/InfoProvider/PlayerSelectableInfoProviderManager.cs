﻿using System;
using ASD.Colony.Core;
using ASD.Colony.Core.Abstract;
using ASD.Colony.UI.Player.Interaction.InfoProvider.Abstract;
using UnityEngine;

namespace ASD.Colony.UI.Player.Interaction.InfoProvider
{
  public class PlayerSelectableInfoProviderManager : ManagerBase<Type, IPlayerSelectableInfoProvider>
  {
    public static PlayerSelectableInfoProviderManager Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<PlayerSelectableInfoProviderManager>();
    }
  }
}
