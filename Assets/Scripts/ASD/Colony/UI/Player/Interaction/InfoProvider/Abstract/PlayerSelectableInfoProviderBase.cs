﻿using ASD.Colony.Core.Player.Interaction.Abstract;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace ASD.Colony.UI.Player.Interaction.InfoProvider.Abstract
{
  public abstract class PlayerSelectableInfoProviderBase<TPlayerSelectable> : IPlayerSelectableInfoProvider<TPlayerSelectable>
    where TPlayerSelectable : IPlayerSelectable
  {
    public Transform ContentRoot { get; set; }
    public Text ObjectNameText { get; set; }
    public Image ObjectSprite { get; set; }
    public Image ObjectHealthBar { get; set; }

    IPlayerSelectable IPlayerSelectableInfoProvider.PlayerSelectable => PlayerSelectable;

    void IPlayerSelectableInfoProvider.Bind(IPlayerSelectable playerSelectable)
    {
      Bind((TPlayerSelectable) playerSelectable);
    }

    public TPlayerSelectable PlayerSelectable { get; private set; }

    public void Bind(TPlayerSelectable playerSelectable)
    {
      PlayerSelectable = playerSelectable;
      OnBind();
    }

    public void Unbind()
    {
      OnUnbind();
      PlayerSelectable = default;
    }

    protected abstract void OnBind();

    protected abstract void OnUnbind();
  }
}
