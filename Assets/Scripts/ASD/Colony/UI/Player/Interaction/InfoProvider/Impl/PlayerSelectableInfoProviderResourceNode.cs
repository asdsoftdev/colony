﻿using System.Globalization;
using ASD.Colony.Core.Extensions;
using ASD.Colony.Core.Resources;
using ASD.Colony.UI.Player.Interaction.InfoProvider.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace ASD.Colony.UI.Player.Interaction.InfoProvider.Impl
{
  public class PlayerSelectableInfoProviderResourceNode : PlayerSelectableInfoProviderBase<ResourceNode>
  {
    private readonly GameObject prefab;

    public PlayerSelectableInfoProviderResourceNode() => prefab = Resources.Load<GameObject>("Prefabs/UI/PlayerSelectableInfoProviders/UnitStat");

    protected override void OnBind()
    {
      ResourceNode node = PlayerSelectable;
      Resource resource = node.Resource;

      ObjectNameText.text = $"{nameof(ResourceNode)} - {resource.ToString()}";
      ObjectSprite.overrideSprite = resource.GetSprite();
      ObjectHealthBar.fillAmount = 1;

      GameObject clone = Object.Instantiate(prefab, ContentRoot);
      Image image = clone.GetComponentInChildren<Image>();
      Text text = clone.GetComponentInChildren<Text>();

      image.overrideSprite = resource.GetSprite();
      text.text = node.Yield.ToString(CultureInfo.InvariantCulture);
    }

    protected override void OnUnbind() { }
  }
}
