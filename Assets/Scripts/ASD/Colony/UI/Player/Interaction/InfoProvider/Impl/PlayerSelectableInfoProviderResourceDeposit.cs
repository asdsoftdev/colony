﻿using ASD.Colony.Core.Buildings;
using ASD.Colony.Core.Extensions;
using ASD.Colony.Core.Resources;
using ASD.Colony.UI.Player.Interaction.InfoProvider.Abstract;

namespace ASD.Colony.UI.Player.Interaction.InfoProvider.Impl
{
  public class PlayerSelectableInfoProviderResourceDeposit : PlayerSelectableInfoProviderBase<ResourceDeposit>
  {
    protected override void OnBind()
    {
      ResourceDeposit deposit = PlayerSelectable;
      Resource resource = deposit.Resource;

      ObjectNameText.text = $"{nameof(ResourceDeposit)} - {resource.ToString()}";
      ObjectSprite.overrideSprite = resource.GetSprite();
      ObjectHealthBar.fillAmount = 1;
    }

    protected override void OnUnbind() { }
  }
}
