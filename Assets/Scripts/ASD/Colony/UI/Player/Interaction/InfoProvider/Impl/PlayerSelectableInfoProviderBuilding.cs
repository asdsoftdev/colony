﻿using ASD.Colony.Core.Buildings;
using ASD.Colony.UI.Player.Interaction.InfoProvider.Abstract;

namespace ASD.Colony.UI.Player.Interaction.InfoProvider.Impl
{
  public class PlayerSelectableInfoProviderBuilding : PlayerSelectableInfoProviderBase<Building>
  {
    protected override void OnBind()
    {
      Building building = PlayerSelectable;

      ObjectNameText.text = nameof(Building);
      ObjectSprite.overrideSprite = null;
      ObjectHealthBar.fillAmount = 1;
    }

    protected override void OnUnbind() { }
  }
}
