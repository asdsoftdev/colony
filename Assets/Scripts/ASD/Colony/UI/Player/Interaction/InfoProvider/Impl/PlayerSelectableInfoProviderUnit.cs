﻿using ASD.Colony.Core.Units;
using ASD.Colony.UI.Player.Interaction.InfoProvider.Abstract;
using ASD.Lib.Core.Properties;
using ASD.Lib.Core.Properties.Abstract;
using UnityEngine;

namespace ASD.Colony.UI.Player.Interaction.InfoProvider.Impl
{
  public class PlayerSelectableInfoProviderUnit : PlayerSelectableInfoProviderBase<Unit>
  {
    protected override void OnBind()
    {
      Unit unit = PlayerSelectable;

      ObjectNameText.text = nameof(Unit);
      ObjectSprite.overrideSprite = Resources.Load<Sprite>("Sprites/Default");
      ObjectHealthBar.fillAmount = unit.HealthController.HealthPercentage;
      unit.HealthController.CurrentHealth.BindOnChanged(unit, UnitOnHealthChanged);
    }

    protected override void OnUnbind()
    {
      Unit unit = PlayerSelectable;

      unit.HealthController.CurrentHealth.UnbindOnChanged(unit, UnitOnHealthChanged);
    }

    private void UnitOnHealthChanged(Unit unit, IMutableProperty<float> property, PropertyChangedEventArgs<float> args)
    {
      ObjectHealthBar.fillAmount = unit.HealthController.HealthPercentage;
    }
  }
}
