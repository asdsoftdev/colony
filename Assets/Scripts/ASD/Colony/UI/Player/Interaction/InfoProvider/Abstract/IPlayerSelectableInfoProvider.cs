﻿using ASD.Colony.Core.Player.Interaction.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace ASD.Colony.UI.Player.Interaction.InfoProvider.Abstract
{
  public interface IPlayerSelectableInfoProvider
  {
    Transform ContentRoot { get; set; }

    Text ObjectNameText { get; set; }

    Image ObjectSprite { get; set; }

    Image ObjectHealthBar { get; set; }

    IPlayerSelectable PlayerSelectable { get; }

    void Bind(IPlayerSelectable playerSelectable);

    void Unbind();
  }

  public interface IPlayerSelectableInfoProvider<TPlayerSelectable> : IPlayerSelectableInfoProvider
    where TPlayerSelectable : IPlayerSelectable
  {
    new TPlayerSelectable PlayerSelectable { get; }

    void Bind(TPlayerSelectable playerSelectable);
  }
}
