﻿using ASD.Colony.Core;
using UnityEngine;

namespace ASD.Colony.UI.BuildMode
{
  public class BuildModeCanvas : MonoBehaviour
  {
    public static BuildModeCanvas Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<BuildModeCanvas>();
    }

    private void Awake()
    {
      GameObject prefab = Resources.Load<GameObject>($"Prefabs/UI/BuildMode/{nameof(BuildModeCanvas)}");
      Instantiate(prefab, transform);
    }
  }
}
