﻿using ASD.Colony.Core.Resources;
using ASD.Lib.Core;
using UnityEngine;
using UnityEngine.UI;

namespace ASD.Colony.UI
{
  public class ResourceDisplayUI : MonoBehaviour
  {
    private Resource resource;
    private float value;

    [SerializeField] private Text resourceText;
    [SerializeField] private Text valueText;

    public Resource Resource
    {
      get => resource;
      set
      {
        resource = value;
        resourceText.text = value.ToString();
      }
    }

    public float Value
    {
      get => value;
      set
      {
        this.value = value;
        valueText.text = MathHelpers.CurrencyString(value);
      }
    }
  }
}
