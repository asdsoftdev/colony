﻿using System;
using System.Linq;
using System.Reflection;
using ASD.Colony.UI.Player.Interaction;
using ASD.Colony.UI.Player.Interaction.InfoProvider;
using ASD.Colony.UI.Player.Interaction.InfoProvider.Abstract;
using UnityEngine;

namespace ASD.Colony.UI
{
  public static class Init
  {
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void InitializeBeforeSceneLoad() { }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    private static void InitializeAfterSceneLoad()
    {
      RegisterPlayerSelectableInfoProviders();
    }

    private static void RegisterPlayerSelectableInfoProviders()
    {
      Type[] exportedHandlers = AppDomain.CurrentDomain.GetAssemblies()
        .SelectMany(assembly => assembly.ExportedTypes)
        .Where(type => type.IsClass && !type.IsAbstract)
        .Where(cls => cls.GetInterface(typeof(IPlayerSelectableInfoProvider<>).FullName) != null)
        .ToArray();

      foreach (Type handlerType in exportedHandlers)
      {
        Type handlerInterface = handlerType.GetInterface(typeof(IPlayerSelectableInfoProvider<>).FullName);

        ConstructorInfo constructor = handlerType.GetConstructor(Type.EmptyTypes);
        Debug.Assert(constructor != null, nameof(constructor) + " != null");

        IPlayerSelectableInfoProvider instance = (IPlayerSelectableInfoProvider) constructor.Invoke(null);
        Type objectType = handlerInterface.GenericTypeArguments[0];
        PlayerSelectableInfoProviderManager.Instance.Register(objectType, instance);
      }
    }
  }
}
