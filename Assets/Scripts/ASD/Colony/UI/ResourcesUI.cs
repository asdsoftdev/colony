﻿using System.Collections.Generic;
using ASD.Colony.Core.Resources;
using ASD.Lib.Core.Properties;
using ASD.Lib.Core.Properties.Abstract;
using UnityEngine;

namespace ASD.Colony.UI
{
  public class ResourcesUI : MonoBehaviour
  {
    private readonly Dictionary<Resource, ResourceDisplayUI> displays = new Dictionary<Resource, ResourceDisplayUI>();

    [SerializeField] private ResourceDisplayUI displayPrefab;

    private void Awake()
    {
      foreach (KeyValuePair<Resource, IMutableProperty<float>> pair in ResourceManager.Instance.GetAll())
      {
        Resource resource = pair.Key;
        IMutableProperty<float> property = pair.Value;

        ResourceDisplayUI display = Instantiate(displayPrefab, transform);
        display.Resource = resource;
        display.Value = property.Value;
        displays[resource] = display;

        property.BindOnChanged(resource, OnResourceValueChanged);
      }
    }

    private void OnResourceValueChanged(Resource resource, IMutableProperty<float> property, PropertyChangedEventArgs<float> args)
    {
      displays[resource].Value = args.NewValue;
    }
  }
}
