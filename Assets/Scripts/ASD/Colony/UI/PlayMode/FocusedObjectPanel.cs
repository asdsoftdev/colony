﻿using ASD.Colony.Core.Player.Interaction;
using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Colony.UI.Player.Interaction.InfoProvider;
using ASD.Colony.UI.Player.Interaction.InfoProvider.Abstract;
using ASD.Lib.Core.Properties;
using ASD.Lib.Core.Properties.Abstract;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace ASD.Colony.UI.PlayMode
{
  public class FocusedObjectPanel : MonoBehaviour
  {
    [SerializeField] private Transform contentRoot;
    [SerializeField] private Text objectNameText;
    [SerializeField] private Image objectSprite;
    [SerializeField] private Image objectHealthBar;

    [CanBeNull] private IPlayerSelectableInfoProvider currentProvider;

    private void OnEnable()
    {
      PlayerInteractionManager.Instance.Selection.FocusedItem.Changed += FocusedItemOnChanged;
    }

    private void OnDisable()
    {
      PlayerInteractionManager.Instance.Selection.FocusedItem.Changed -= FocusedItemOnChanged;
    }

    private void FocusedItemOnChanged(IMutableProperty<IPlayerInteractable> property, PropertyChangedEventArgs<IPlayerInteractable> args)
    {
      ClearContent();
      IPlayerInteractable focusedObject = args.NewValue;
      if (focusedObject == null)
        return;

      IPlayerSelectableInfoProvider infoProvider = PlayerSelectableInfoProviderManager.Instance.Get(focusedObject.GetType());
      currentProvider?.Unbind();
      currentProvider = infoProvider;
      if (infoProvider == null)
        return;

      infoProvider.ContentRoot = contentRoot;
      infoProvider.ObjectNameText = objectNameText;
      infoProvider.ObjectSprite = objectSprite;
      infoProvider.ObjectHealthBar = objectHealthBar;
      infoProvider.Bind(focusedObject);
    }

    private void ClearContent()
    {
      for (int i = 0; i < contentRoot.childCount; i++)
        Destroy(contentRoot.GetChild(i).gameObject);

      objectNameText.text = "";
      objectSprite.overrideSprite = null;
      objectHealthBar.fillAmount = 1;
    }
  }
}
