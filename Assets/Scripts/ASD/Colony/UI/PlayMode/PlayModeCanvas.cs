﻿using ASD.Colony.Core;
using UnityEngine;

namespace ASD.Colony.UI.PlayMode
{
  public class PlayModeCanvas : MonoBehaviour
  {
    public static PlayModeCanvas Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<PlayModeCanvas>();
    }

    private void Awake()
    {
      GameObject prefab = Resources.Load<GameObject>($"Prefabs/UI/PlayMode/{nameof(PlayModeCanvas)}");
      Instantiate(prefab, transform);
    }
  }
}
