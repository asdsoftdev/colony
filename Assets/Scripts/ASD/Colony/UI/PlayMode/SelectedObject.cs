﻿using UnityEngine;
using UnityEngine.UI;

namespace ASD.Colony.UI.PlayMode
{
  public class SelectedObject : MonoBehaviour
  {
    public Button Button { get; private set; }
    public Image Sprite { get; private set; }
    public Image FocusBorderSprite { get; private set; }

    private void Awake()
    {
      Button = GetComponent<Button>();
      Image[] images = GetComponentsInChildren<Image>(true);
      Sprite = images[0];
      FocusBorderSprite = images[1];
    }
  }
}
