﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ASD.Colony.Core.Extensions;
using ASD.Colony.Core.Player.Interaction;
using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Colony.Core.Resources;
using ASD.Colony.Core.Units;
using ASD.Lib.Core.Properties;
using ASD.Lib.Core.Properties.Abstract;
using UnityEngine;

namespace ASD.Colony.UI.PlayMode
{
  public class SelectedObjectsPanel : MonoBehaviour
  {
    [SerializeField] private SelectedObject objectUIPrefab;
    [SerializeField] private Transform viewport;

    private readonly IDictionary<IPlayerInteractable, SelectedObject> selectedObjects = new Dictionary<IPlayerInteractable, SelectedObject>();

    private void OnEnable()
    {
      PlayerInteractionManager.Instance.Selection.SelectedItems.CollectionChanged += SelectedItemsOnCollectionChanged;
      PlayerInteractionManager.Instance.Selection.FocusedItem.Changed += FocusedItemOnChanged;
    }

    private void OnDisable()
    {
      PlayerInteractionManager.Instance.Selection.SelectedItems.CollectionChanged -= SelectedItemsOnCollectionChanged;
      PlayerInteractionManager.Instance.Selection.FocusedItem.Changed -= FocusedItemOnChanged;
    }

    private SelectedObject CreateSelectedObjectUI(IPlayerInteractable interactable)
    {
      GameObject go = Instantiate(objectUIPrefab.gameObject, viewport);
      SelectedObject selectedObject = go.GetComponent<SelectedObject>();

      selectedObject.Sprite.overrideSprite = GetSpriteForObject(interactable);
      selectedObject.Button.onClick.AddListener(() => SelectedObjectOnButtonClicked(interactable));

      selectedObjects.Add(interactable, selectedObject);

      return selectedObject;
    }

    private void SelectedItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          foreach (IPlayerInteractable interactable in e.NewItems)
          {
            if (selectedObjects.ContainsKey(interactable))
              continue;

            CreateSelectedObjectUI(interactable);
          }

          break;
        case NotifyCollectionChangedAction.Move:
          break;
        case NotifyCollectionChangedAction.Remove:
          break;
        case NotifyCollectionChangedAction.Replace:
          break;
        case NotifyCollectionChangedAction.Reset:
          foreach (KeyValuePair<IPlayerInteractable, SelectedObject> pair in selectedObjects)
            Destroy(pair.Value.gameObject);

          selectedObjects.Clear();

          break;
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    private void SelectedObjectOnButtonClicked(IPlayerInteractable interactable)
    {
      PlayerInteractionManager.Instance.Selection.SetFocus(interactable);
    }

    private void FocusedItemOnChanged(IMutableProperty<IPlayerInteractable> property, PropertyChangedEventArgs<IPlayerInteractable> args)
    {
      if (args.OldValue != null)
        SetFocused(args.OldValue, false);
      if (args.NewValue != null)
        SetFocused(args.NewValue, true);
    }

    private void SetFocused(IPlayerInteractable interactable, bool focused)
    {
      SelectedObject selectedObject = selectedObjects.ContainsKey(interactable) ? selectedObjects[interactable] : CreateSelectedObjectUI(interactable);

      selectedObject.FocusBorderSprite.enabled = focused;
    }

    private static Sprite GetSpriteForObject(object obj)
    {
      switch (obj)
      {
        case Unit _:
          return Resources.Load<Sprite>("Sprites/Units/Unit");

        case ResourceNode resourceNode:
          return resourceNode.Resource.GetSprite();
      }

      return Resources.Load<Sprite>("Sprites/Default");
    }
  }
}
