﻿using UnityEngine;
using UnityEngine.UI;

namespace ASD.Colony.UI
{
  [RequireComponent(typeof(Image))]
  public class SelectionRectangleUI : MonoBehaviour
  {
    private Image image;
    private RectTransform imageTransform;

    private void Awake()
    {
      image = GetComponent<Image>();
      imageTransform = image.rectTransform;
    }

    public void Hide()
    {
      image.enabled = false;
    }

    public void Show()
    {
      image.enabled = true;
    }

    public void SetRect(Rect rect)
    {
      Vector2 scale = imageTransform.lossyScale;

      imageTransform.position = new Vector3(rect.x, rect.y);
      imageTransform.sizeDelta = new Vector2(rect.width / scale.x, rect.height / scale.y);
    }
  }
}
