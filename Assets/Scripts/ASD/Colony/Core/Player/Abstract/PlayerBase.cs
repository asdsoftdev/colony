﻿using ASD.Colony.Core.Teams;
using UnityEngine;

namespace ASD.Colony.Core.Player.Abstract
{
  public abstract class PlayerBase : MonoBehaviour, IPlayer
  {
    public Team Team { get; set; }

    protected virtual void Awake()
    {
      TeamManager.Instance.GetOrCreateTeam(this);
    }
  }
}
