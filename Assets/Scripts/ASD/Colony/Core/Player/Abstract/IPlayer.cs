﻿using ASD.Colony.Core.Teams;

namespace ASD.Colony.Core.Player.Abstract
{
  public interface IPlayer
  {
    Team Team { get; set; }
  }
}
