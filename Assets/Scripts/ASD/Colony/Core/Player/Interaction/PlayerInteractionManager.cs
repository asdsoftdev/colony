﻿using System;
using ASD.Colony.Core.Abstract;
using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Colony.Core.Player.Selection;
using ASD.Colony.Core.Player.Selection.Abstract;
using UnityEngine;

namespace ASD.Colony.Core.Player.Interaction
{
  public class PlayerInteractionManager : ManagerBase<Type, Type, IPlayerInteractableHandler>
  {
    public static PlayerInteractionManager Instance { get; private set; }

    public IPlayerSelection Selection { get; } = new PlayerSelection();

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<PlayerInteractionManager>();
    }

    public void Handle(IPlayerInteractable subject, IPlayerInteractable context)
    {
      IPlayerInteractableHandler handler = Get(subject.GetType(), context.GetType());
      handler?.Handle(subject, context);
    }

    #if UNITY_EDITOR
    private void OnDrawGizmos()
    {
      Gizmos.color = Color.blue;
      foreach (IPlayerInteractable selectable in Instance.Selection.SelectedItems)
      {
        Component component = (Component) selectable;
        Transform t = component.transform;
        MeshFilter meshFilter = component.GetComponent<MeshFilter>();
        if (meshFilter)
          Gizmos.DrawWireMesh(meshFilter.mesh, t.position, t.rotation, t.lossyScale * 1.1f);
        else
          Gizmos.DrawWireSphere(t.position, t.lossyScale.magnitude * 1.1f);
      }
    }
    #endif
  }
}
