﻿using ASD.Colony.Core.Player.Abstract;

namespace ASD.Colony.Core.Player.Interaction.Abstract
{
  public interface IPlayerOwnable
  {
    IPlayer Owner { get; set; }
  }
}
