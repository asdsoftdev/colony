﻿using JetBrains.Annotations;

namespace ASD.Colony.Core.Player.Interaction.Abstract
{
  [UsedImplicitly(ImplicitUseKindFlags.InstantiatedWithFixedConstructorSignature,
    ImplicitUseTargetFlags.Default | (ImplicitUseTargetFlags) 4)]
  public interface IPlayerInteractableHandler
  {
    void Handle(IPlayerInteractable subject, IPlayerInteractable context);
  }

  public interface IPlayerInteractableHandler<in TSubject, in TContext> : IPlayerInteractableHandler
    where TSubject : IPlayerInteractable
    where TContext : IPlayerInteractable
  {
    void Handle(TSubject subject, TContext context);
  }
}
