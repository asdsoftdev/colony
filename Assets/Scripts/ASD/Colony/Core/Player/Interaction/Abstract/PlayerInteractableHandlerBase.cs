﻿namespace ASD.Colony.Core.Player.Interaction.Abstract
{
  public abstract class PlayerInteractableHandlerBase<TSubject, TContext> : IPlayerInteractableHandler<TSubject, TContext>
    where TSubject : IPlayerInteractable
    where TContext : IPlayerInteractable
  {
    public abstract void Handle(TSubject subject, TContext context);

    void IPlayerInteractableHandler.Handle(IPlayerInteractable subject, IPlayerInteractable context)
    {
      Handle((TSubject) subject, (TContext) context);
    }
  }
}
