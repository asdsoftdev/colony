﻿namespace ASD.Colony.Core.Player.Interaction.Abstract
{
  public interface IPlayerInteractable : IPlayerOwnable, IPlayerSelectable { }
}
