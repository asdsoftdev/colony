﻿namespace ASD.Colony.Core.Player.Interaction.Abstract
{
  public interface IPlayerSelectable
  {
    bool IsSelected { get; }

    void Select();

    void Deselect();
  }
}
