﻿using System;
using ASD.Colony.Core.Player.Abstract;
using UnityEngine;

namespace ASD.Colony.Core.Player.Interaction.Abstract
{
  public abstract class PlayerInteractableBase : MonoBehaviour, IPlayerInteractable
  {
    [SerializeField] private Renderer selectionRenderer;
    private IPlayer owner;

    public IPlayer Owner
    {
      get
      {
        if (owner is null)
          throw new InvalidOperationException();

        return owner;
      }
      set
      {
        if (!(owner is null))
          throw new InvalidOperationException();

        owner = value;
      }
    }

    private bool playerInteractableIsSelected;
    bool IPlayerSelectable.IsSelected => playerInteractableIsSelected;

    void IPlayerSelectable.Select()
    {
      SetSelected(true);
    }

    void IPlayerSelectable.Deselect()
    {
      SetSelected(false);
    }

    private void SetSelected(bool selected)
    {
      playerInteractableIsSelected = selected;
      selectionRenderer.enabled = selected;
    }

    #if UNITY_EDITOR
    protected virtual void Reset()
    {
      selectionRenderer = transform.Find("SelectionRenderer")?.GetComponent<Renderer>();
      if (selectionRenderer)
        return;

      GameObject selectionGo = new GameObject("SelectionRenderer");
      selectionGo.transform.SetParent(transform);

      SpriteRenderer spriteRenderer;
      selectionRenderer = spriteRenderer = selectionGo.AddComponent<SpriteRenderer>();
      selectionRenderer.transform.position = Vector3.up * 0.1f;
      selectionRenderer.transform.rotation = Quaternion.Euler(-90, 0, 0);
      selectionRenderer.enabled = false;

      spriteRenderer.sprite = UnityEngine.Resources.Load<Sprite>("Sprites/UnitSelectionBorder");

      gameObject.layer = LayerMask.NameToLayer("PlayerSelectable");
    }
    #endif
  }
}
