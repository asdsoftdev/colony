﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Colony.Core.Player.Selection.Abstract;
using ASD.Colony.Core.Properties;
using ASD.Lib.Core.Collections;
using ASD.Lib.Core.Properties.Abstract;
using JetBrains.Annotations;

namespace ASD.Colony.Core.Player.Selection
{
  public class PlayerSelection : IPlayerSelection
  {
    private readonly IWriteableProperty<IPlayerInteractable> focusedItem = new WriteablePropertyIPlayerInteractable();

    public IObservableSet<IPlayerInteractable> SelectedItems { get; }

    /// <summary>
    /// null when SelectedItems is empty.
    /// </summary>
    [CanBeNull]
    public IMutableProperty<IPlayerInteractable> FocusedItem => focusedItem;

    public PlayerSelection() : this(Enumerable.Empty<IPlayerInteractable>()) { }

    public PlayerSelection(IPlayerInteractable selectable) : this(new[] {selectable}) { }

    public PlayerSelection(IEnumerable<IPlayerInteractable> selectables)
    {
      SelectedItems = new ObservableHashSet<IPlayerInteractable>();
      SelectedItems.CollectionChanged += SelectedItemsOnCollectionChanged;
    }

    public void Select(params IPlayerInteractable[] interactables)
    {
      foreach (IPlayerInteractable interactable in interactables)
      {
        interactable.Select();
        SelectedItems.Add(interactable);
      }
    }

    public void Deselect(params IPlayerInteractable[] interactables)
    {
      foreach (IPlayerInteractable interactable in interactables)
      {
        interactable.Deselect();
        SelectedItems.Remove(interactable);
      }
    }

    public void DeselectAll()
    {
      foreach (IPlayerInteractable interactable in SelectedItems)
        interactable.Deselect();
      SelectedItems.Clear();
    }

    public void SetFocus(IPlayerInteractable focusedItem)
    {
      if (!SelectedItems.Contains(focusedItem))
        Select(focusedItem);
      this.focusedItem.Value = focusedItem;
    }

    private void SelectedItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (SelectedItems.Contains(focusedItem.Value))
        return;

      IPlayerInteractable newFocus = SelectedItems.FirstOrDefault();
      if (newFocus is null)
        focusedItem.Value = null;
      else
        SetFocus(newFocus);
    }
  }
}
