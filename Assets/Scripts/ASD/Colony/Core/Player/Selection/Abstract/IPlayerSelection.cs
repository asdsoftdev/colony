﻿using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Lib.Core.Collections;
using ASD.Lib.Core.Properties.Abstract;

namespace ASD.Colony.Core.Player.Selection.Abstract
{
  public interface IPlayerSelection
  {
    IObservableSet<IPlayerInteractable> SelectedItems { get; }

    IMutableProperty<IPlayerInteractable> FocusedItem { get; }

    void Select(params IPlayerInteractable[] interactables);

    void Deselect(params IPlayerInteractable[] interactables);

    void DeselectAll();

    void SetFocus(IPlayerInteractable focusedItem);
  }
}
