﻿using ASD.Colony.Core.Player.Abstract;
using UnityEngine;

namespace ASD.Colony.Core.Player
{
  public class HumanPlayer : PlayerBase
  {
    public static HumanPlayer Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<HumanPlayer>();
    }
  }
}
