﻿using System;
using ASD.Colony.Core.Character.Stats.Abstract;
using ASD.Lib.Core.Properties.Abstract;

namespace ASD.Colony.Core.Character.Stats.Impl
{
  [Serializable]
  public sealed class RangeStat : WriteablePropertyBase<float>, IStat<float>
  {
    public RangeStat() : base(1f) { }
  }
}
