﻿using ASD.Lib.Core.Properties.Abstract;

namespace ASD.Colony.Core.Character.Stats.Abstract
{
  public interface IStat<out T> : IProperty<T> { }
}
