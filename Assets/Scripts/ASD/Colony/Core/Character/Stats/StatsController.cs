﻿using System;
using System.Collections.Generic;
using System.Linq;
using ASD.Colony.Core.Character.Stats.Abstract;
using UnityEngine;

namespace ASD.Colony.Core.Character.Stats
{
  public class StatsController : MonoBehaviour
  {
    public static IEnumerable<Type> ExportedStatTypes => exportedStatTypes;
    private static Type[] exportedStatTypes;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      exportedStatTypes = AppDomain.CurrentDomain
        .GetAssemblies()
        .SelectMany(assembly => assembly.GetExportedTypes())
        .Where(type => type.GetInterfaces().Any(i => i.IsConstructedGenericType && i.GetGenericTypeDefinition() == typeof(IStat<>)))
        .ToArray();
    }

    private readonly Dictionary<Type, object> stats = new Dictionary<Type, object>();

    private void Start()
    {
      foreach (Type type in exportedStatTypes.Where(type => !stats.ContainsKey(type)))
      {
        // ReSharper disable once PossibleNullReferenceException
        object stat = type.GetConstructor(Type.EmptyTypes).Invoke(null);
        Add(stat);
      }
    }

    private void Add<T>(IStat<T> stat)
    {
      Add((object) stat);
    }

    private void Add(object stat)
    {
      Type type = stat.GetType();
      if (!type.GetInterfaces().Any(i => i.IsConstructedGenericType && i.GetGenericTypeDefinition() == typeof(IStat<>)))
        throw new ArgumentException("stat is not an instance of IStat<T>", nameof(stat));

      if (stats.ContainsKey(type))
        throw new InvalidOperationException($"There's already a registered stat with that type: {type.Name}");

      stats[type] = stat;
    }

    public TStat Get<TStat>() => (TStat) Get(typeof(TStat));

    // public TStat Get<TStat, T>() where TStat : IStat<T> => (TStat) Get(typeof(TStat));

    public object Get(Type type) => stats[type];

    public IReadOnlyDictionary<Type, object> GetAll() => stats;
  }
}
