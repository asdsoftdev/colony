﻿using System.Collections;
using ASD.Colony.Core.Extensions;
using UnityEngine;
using UnityEngine.AI;

namespace ASD.Colony.Core.Character
{
  [RequireComponent(typeof(NavMeshAgent))]
  public class CharacterController3 : MonoBehaviour
  {
    private const string AnimatorMoveSpeedParam = "movementSpeed";
    private static readonly int AnimatorMoveSpeedHash = Animator.StringToHash(AnimatorMoveSpeedParam);

    private NavMeshAgent agent;
    private Animator animator;

    public NavMeshAgent Agent => agent;

    private void Awake()
    {
      agent = GetComponent<NavMeshAgent>();
      animator = GetComponentInChildren<Animator>();
    }

    private IEnumerator WaitForAgentStopCoroutine()
    {
      while
      (agent.pathPending ||
       agent.remainingDistance > agent.stoppingDistance ||
       agent.hasPath && !Mathf.Approximately(agent.velocity.sqrMagnitude, 0f))
        yield return null;

      yield return Stop().StartAsCoroutine();
    }

    public IEnumerator MoveTo(Vector3 worldPosition)
    {
      agent.SetDestination(worldPosition);
      agent.isStopped = false;
      animator.SetFloat(AnimatorMoveSpeedHash, 1f);
      yield return WaitForAgentStopCoroutine().StartAsCoroutine();
    }

    public IEnumerator Stop()
    {
      agent.isStopped = true;
      agent.ResetPath();
      animator.SetFloat(AnimatorMoveSpeedHash, 0f);
      yield break;
    }

    public IEnumerator LookAt(Vector3 worldPosition)
    {
      transform.LookAt(worldPosition, transform.up);
      yield break;
    }
  }
}
