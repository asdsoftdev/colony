﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.Annotations;
using UnityEngine;

namespace ASD.Colony.Core.Abstract
{
  public abstract class ManagerBase<T> : MonoBehaviour, IManager<T>
  {
    // ReSharper disable once InconsistentNaming
    protected readonly ICollection<T> registeredObjects;
    public IEnumerable<T> RegisteredObjects => registeredObjects;

    protected ManagerBase(ICollection<T> collection) => registeredObjects = collection;

    public bool Register(T value)
    {
      registeredObjects.Add(value);

      return true;
    }

    public bool Unregister(T value) => registeredObjects.Remove(value);
  }

  public abstract class ManagerBase<TIn, TValue> : MonoBehaviour, IManager<TIn, ManagerBase<TIn, TValue>.Key, TValue>
  {
    // ReSharper disable once InconsistentNaming
    protected readonly IDictionary<Key, TValue> registeredObjectsMap;
    public IReadOnlyDictionary<Key, TValue> RegisteredObjectsMap => new ReadOnlyDictionary<Key, TValue>(registeredObjectsMap);

    protected ManagerBase() : this(new Dictionary<Key, TValue>()) { }

    protected ManagerBase(IDictionary<Key, TValue> dictionary) => registeredObjectsMap = dictionary;

    public bool Register(TIn t, TValue value)
    {
      Key key = MakeKey(t);
      if (registeredObjectsMap.ContainsKey(key))
        return false;

      registeredObjectsMap[key] = value;

      return true;
    }

    public bool Unregister(TIn t)
    {
      Key key = MakeKey(t);

      return registeredObjectsMap.Remove(key);
    }

    [CanBeNull]
    public TValue Get(TIn t)
    {
      Key key = MakeKey(t);
      // ReSharper disable once ConvertIfStatementToReturnStatement
      if (registeredObjectsMap.TryGetValue(key, out TValue value))
        return value;

      return default;
    }

    public Key MakeKey(TIn t) => new Key(t);

    public class Key : IEquatable<Key>
    {
      public TIn In { get; }

      public Key(TIn @in) => In = @in;

      public bool Equals(Key other)
      {
        if (ReferenceEquals(null, other))
          return false;
        // ReSharper disable once ConvertIfStatementToReturnStatement
        if (ReferenceEquals(this, other))
          return true;

        return EqualityComparer<TIn>.Default.Equals(In, other.In);
      }

      public override bool Equals(object obj)
      {
        if (ReferenceEquals(null, obj))
          return false;
        if (ReferenceEquals(this, obj))
          return true;

        return obj.GetType() == GetType() && Equals((Key) obj);
      }

      public override int GetHashCode() => EqualityComparer<TIn>.Default.GetHashCode(In);

      public static bool operator ==(Key left, Key right) => Equals(left, right);

      public static bool operator !=(Key left, Key right) => !Equals(left, right);
    }
  }

  public abstract class ManagerBase<TIn1, TIn2, TValue> : MonoBehaviour, IManager<TIn1, TIn2, ManagerBase<TIn1, TIn2, TValue>.Key, TValue>
  {
    // ReSharper disable once InconsistentNaming
    protected readonly IDictionary<Key, TValue> registeredObjectsMap;
    public IReadOnlyDictionary<Key, TValue> RegisteredObjectsMap => new ReadOnlyDictionary<Key, TValue>(registeredObjectsMap);

    protected ManagerBase() : this(new Dictionary<Key, TValue>()) { }

    protected ManagerBase(IDictionary<Key, TValue> dictionary) => registeredObjectsMap = dictionary;

    public bool Register(TIn1 t1, TIn2 t2, TValue value)
    {
      Key key = MakeKey(t1, t2);
      if (registeredObjectsMap.ContainsKey(key))
        return false;

      registeredObjectsMap[key] = value;

      return true;
    }

    public bool Unregister(TIn1 t1, TIn2 t2)
    {
      Key key = MakeKey(t1, t2);

      return registeredObjectsMap.Remove(key);
    }

    [CanBeNull]
    public TValue Get(TIn1 t1, TIn2 t2)
    {
      Key key = MakeKey(t1, t2);
      // ReSharper disable once ConvertIfStatementToReturnStatement
      if (registeredObjectsMap.TryGetValue(key, out TValue value))
        return value;

      return default;
    }

    public Key MakeKey(TIn1 t1, TIn2 t2) => new Key(t1, t2);

    public class Key : IEquatable<Key>
    {
      public TIn1 In1 { get; }
      public TIn2 In2 { get; }

      public Key(TIn1 t1, TIn2 t2)
      {
        In1 = t1;
        In2 = t2;
      }

      public bool Equals(Key other)
      {
        if (ReferenceEquals(null, other))
          return false;
        if (ReferenceEquals(this, other))
          return true;

        return EqualityComparer<TIn1>.Default.Equals(In1, other.In1) && EqualityComparer<TIn2>.Default.Equals(In2, other.In2);
      }

      public override bool Equals(object obj)
      {
        if (ReferenceEquals(null, obj))
          return false;
        if (ReferenceEquals(this, obj))
          return true;

        return obj.GetType() == GetType() && Equals((Key) obj);
      }

      public override int GetHashCode()
      {
        unchecked
        {
          return ((In1 != null ? In1.GetHashCode() : 0) * 397) ^ (In2 != null ? In2.GetHashCode() : 0);
        }
      }

      public static bool operator ==(Key left, Key right) => Equals(left, right);

      public static bool operator !=(Key left, Key right) => !Equals(left, right);
    }
  }
}
