﻿using System;
using System.Collections.Generic;

namespace ASD.Colony.Core.Abstract
{
  public interface IManager<T>
  {
    IEnumerable<T> RegisteredObjects { get; }

    bool Register(T value);

    bool Unregister(T value);
  }

  public interface IManager<TKey, TValue>
    where TKey : IEquatable<TKey>
  {
    IReadOnlyDictionary<TKey, TValue> RegisteredObjectsMap { get; }
  }

  public interface IManager<in TIn, TKey, TValue> : IManager<TKey, TValue>
    where TKey : IEquatable<TKey>
  {
    bool Register(TIn t, TValue value);

    bool Unregister(TIn t);

    TValue Get(TIn t);

    TKey MakeKey(TIn t);
  }

  public interface IManager<in TIn1, in TIn2, TKey, TValue> : IManager<TKey, TValue>
    where TKey : IEquatable<TKey>
  {
    bool Register(TIn1 t1, TIn2 t2, TValue value);

    bool Unregister(TIn1 t1, TIn2 t2);

    TValue Get(TIn1 t1, TIn2 t2);

    TKey MakeKey(TIn1 t1, TIn2 t2);
  }
}
