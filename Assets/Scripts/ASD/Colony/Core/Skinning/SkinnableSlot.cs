﻿namespace ASD.Colony.Core.Skinning
{
  public enum SkinnableSlot
  {
    LeftHand,
    RightHand
  }
}