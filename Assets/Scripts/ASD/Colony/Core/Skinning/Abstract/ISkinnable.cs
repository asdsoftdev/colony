﻿namespace ASD.Colony.Core.Skinning.Abstract
{
  public interface ISkinnable
  {
    void Equip(SkinnableSlot slot, ISkinnableItem item);

    void Unequip(SkinnableSlot slot);
  }
}
