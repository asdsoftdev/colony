﻿using System.Collections.Generic;

namespace ASD.Colony.Core.Skinning.Abstract
{
  public interface ISkinnableItemSupplier
  {
    IReadOnlyDictionary<SkinnableSlot, ISkinnableItem> SupplyFor(ISkinnable skinnable);
  }
}