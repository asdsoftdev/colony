﻿using System;
using UnityEngine;

namespace ASD.Colony.Core.Skinning
{
  [Serializable]
  public class SkinnableSlotMapping
  {
    [SerializeField] private SkinnableSlot slot;
    [SerializeField] private Transform transform;

    public SkinnableSlot Slot => slot;
    public Transform Transform => transform;
  }
}
