﻿using System;
using System.Linq;
using System.Reflection;
using ASD.Colony.Core.Player.Interaction;
using ASD.Colony.Core.Player.Interaction.Abstract;
using UnityEngine;

namespace ASD.Colony.Core
{
  public static class Init
  {
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void InitializeBeforeSceneLoad() { }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    private static void InitializeAfterSceneLoad()
    {
      RegisterPlayerInteractableHandlers();
    }

    private static void RegisterPlayerInteractableHandlers()
    {
      Type[] exportedHandlers = AppDomain.CurrentDomain.GetAssemblies()
        .SelectMany(assembly => assembly.ExportedTypes)
        .Where(type => type.IsClass && !type.IsAbstract)
        .Where(cls => cls.GetInterface(typeof(IPlayerInteractableHandler<,>).FullName) != null)
        .ToArray();

      foreach (Type handlerType in exportedHandlers)
      {
        Type handlerInterface = handlerType.GetInterface(typeof(IPlayerInteractableHandler<,>).FullName);

        ConstructorInfo constructor = handlerType.GetConstructor(Type.EmptyTypes);
        Debug.Assert(constructor != null, nameof(constructor) + " != null");

        IPlayerInteractableHandler instance = (IPlayerInteractableHandler) constructor.Invoke(null);
        Type subjectType = handlerInterface.GenericTypeArguments[0];
        Type contextType = handlerInterface.GenericTypeArguments[1];
        PlayerInteractionManager.Instance.Register(subjectType, contextType, instance);
      }
    }
  }
}
