﻿using System.Collections.Generic;

namespace ASD.Colony.Core.Extensions
{
  public static class CollectionExtensions
  {
    public static void ToggleContainmentOf<T>(this ICollection<T> collection, T obj)
    {
      if (collection.Contains(obj))
        collection.Remove(obj);
      else
        collection.Add(obj);
    }
  }
}
