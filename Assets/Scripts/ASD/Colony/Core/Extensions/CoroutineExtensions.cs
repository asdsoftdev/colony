﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace ASD.Colony.Core.Extensions
{
  public static class CoroutineExtensions
  {
    public static Coroutine StartAsCoroutine(this IEnumerator coroutine) =>
      coroutine.StartAsCoroutine(new Scheduler.CancellationToken());

    public static Coroutine StartAsCoroutine(this IEnumerator coroutine, Scheduler.CancellationToken token) =>
      Scheduler.Instance.StartCoroutineSingle(coroutine, token);

    public static Coroutine ChainAsCoroutine(this IEnumerator coroutine, params IEnumerator[] coroutines) =>
      coroutine.ChainAsCoroutine(new Scheduler.CancellationToken(), coroutines);

    public static Coroutine ChainAsCoroutine(this IEnumerator coroutine, Scheduler.CancellationToken token, params IEnumerator[] coroutines) =>
      Scheduler.Instance.ChainCoroutines(token, coroutines.Prepend(coroutine).ToArray());
  }
}
