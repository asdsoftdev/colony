﻿using ASD.Colony.Core.Resources;
using UnityEngine;

namespace ASD.Colony.Core.Extensions
{
  public static class ResourceExtensions
  {
    public static Sprite GetSprite(this Resource resource) => UnityEngine.Resources.Load<Sprite>($"Sprites/Resource/{resource}");
  }
}
