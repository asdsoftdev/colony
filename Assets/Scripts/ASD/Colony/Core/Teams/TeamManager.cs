﻿using System;
using System.Collections.Generic;
using System.Linq;
using ASD.Colony.Core.Player.Abstract;
using ASD.Colony.Core.Player.Interaction.Abstract;
using UnityEngine;

namespace ASD.Colony.Core.Teams
{
  public class TeamManager : MonoBehaviour
  {
    public static TeamManager Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<TeamManager>();
    }

    private readonly Dictionary<IPlayer, Team> teams = new Dictionary<IPlayer, Team>();

    public Team GetOrCreateTeam(IPlayer player)
    {
      if (teams.ContainsKey(player))
        return teams[player];

      return teams[player] = new Team(player);
    }

    public void DisbandTeam(Team teamToDisband, Team newTeamForOwner)
    {
      if (teamToDisband.Members.Count() > 1)
        throw new InvalidOperationException("Can't disband team with more than 1 member.");

      teams[teamToDisband.Owner] = newTeamForOwner;
    }

    public bool AreOnSameTeam(IPlayer left, IPlayer right) => GetOrCreateTeam(left) == GetOrCreateTeam(right);

    public bool AreEnemies(IPlayer left, IPlayer right) => !AreOnSameTeam(left, right);

    public bool AreEnemies(IPlayerOwnable left, IPlayerOwnable right) => AreEnemies(left.Owner, right.Owner);
  }
}
