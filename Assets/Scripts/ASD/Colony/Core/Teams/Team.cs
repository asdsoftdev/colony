﻿using System.Collections.Generic;
using ASD.Colony.Core.Player;
using ASD.Colony.Core.Player.Abstract;

namespace ASD.Colony.Core.Teams
{
  public class Team
  {
    private readonly IList<IPlayer> members = new List<IPlayer>();

    public IPlayer Owner { get; }

    public IEnumerable<IPlayer> Members => members;

    public Team(IPlayer owner)
    {
      Owner = owner;
      members.Add(owner);
      Owner.Team = this;
    }
  }
}
