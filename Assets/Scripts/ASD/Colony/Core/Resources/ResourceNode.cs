﻿using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Colony.Core.Units;
using ASD.Lib.Core.Properties.Abstract;
using ASD.Lib.Core.Properties.Primitives;
using JetBrains.Annotations;
using UnityEngine;

namespace ASD.Colony.Core.Resources
{
  public class ResourceNode : PlayerInteractableBase
  {
    [SerializeField] private Resource resource;
    [SerializeField] private float yield = 1f;

    public NodeState State { get; } = new NodeState(null);

    public Resource Resource => resource;
    public float Yield => yield;

    #if UNITY_EDITOR
    private void OnDrawGizmos()
    {
      if (!State.IsBusy.Value)
        return;

      Debug.Assert(State.GatheringUnit != null, "State.GatheringUnit != null");

      Gizmos.color = Color.red;
      Gizmos.DrawLine(transform.position, State.GatheringUnit.transform.position);
    }
    #endif

    public class NodeState
    {
      private readonly IWriteableProperty<bool> isBusy = new WriteablePropertyBool();

      [CanBeNull] private Unit gatheringUnit;

      public NodeState([CanBeNull] Unit gatheringUnit) => GatheringUnit = gatheringUnit;

      public IMutableProperty<bool> IsBusy => isBusy;

      [CanBeNull]
      public Unit GatheringUnit
      {
        get => gatheringUnit;
        set
        {
          gatheringUnit = value;
          isBusy.Value = gatheringUnit;
        }
      }
    }
  }
}
