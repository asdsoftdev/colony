﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using ASD.Lib.Core.Properties.Abstract;
using ASD.Lib.Core.Properties.Primitives;
using UnityEngine;

namespace ASD.Colony.Core.Resources
{
  public class ResourceManager : MonoBehaviour
  {
    private readonly Dictionary<Resource, IWriteableProperty<float>> resources = new Dictionary<Resource, IWriteableProperty<float>>();

    public static ResourceManager Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<ResourceManager>();
    }

    private void Awake()
    {
      foreach (Resource resource in Enum.GetValues(typeof(Resource)))
        resources[resource] = new WriteablePropertyFloat();
    }

    [Pure]
    public float Get(Resource resource) => resources[resource].Value;

    [Pure]
    public IReadOnlyDictionary<Resource, IMutableProperty<float>> GetAll() =>
      resources.ToDictionary(pair => pair.Key, pair => (IMutableProperty<float>) pair.Value);

    [Pure]
    public bool Has(Resource resource, float count) => resources[resource].Value >= count;

    public float Increase(Resource resource, float count)
    {
      if (count < 0)
        throw new ArgumentException("count must be non-negative.", nameof(count));

      return Delta(resource, count);
    }

    public float Decrease(Resource resource, float count)
    {
      if (count < 0)
        throw new ArgumentException("count must be non-negative.", nameof(count));

      if (count > Get(resource))
        throw new ArgumentException("count can't be larger than current number of resource.", nameof(count));

      return Delta(resource, -count);
    }

    private float Delta(Resource resource, float delta)
    {
      return resources[resource].Value += delta;
    }
  }
}
