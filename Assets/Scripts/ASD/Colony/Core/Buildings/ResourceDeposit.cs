﻿using System;
using System.Collections.Generic;
using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Colony.Core.Resources;
using ASD.Colony.Core.Skinning;
using ASD.Colony.Core.Skinning.Abstract;
using ASD.Colony.Core.Units;
using ASD.Colony.Core.Units.Jobs.Impl;
using UnityEngine;

namespace ASD.Colony.Core.Buildings
{
  public class ResourceDeposit : Building, ISkinnableItemSupplier
  {
    [SerializeField] private Resource resource;
    [SerializeField] private GameObject toolPrefab;
    private ISkinnableItem skinnableTool;

    public Resource Resource => resource;

    private void Awake()
    {
      skinnableTool = toolPrefab.GetComponent<ISkinnableItem>();
    }

    public IReadOnlyDictionary<SkinnableSlot, ISkinnableItem> SupplyFor(ISkinnable skinnable)
    {
      // ReSharper disable once ConvertIfStatementToReturnStatement
      if (skinnable is Unit)
        return new Dictionary<SkinnableSlot, ISkinnableItem> {[SkinnableSlot.RightHand] = skinnableTool};

      return new Dictionary<SkinnableSlot, ISkinnableItem>();
    }

    #if UNITY_EDITOR
    private void OnValidate()
    {
      if (toolPrefab && toolPrefab.GetComponent<ISkinnableItem>() == null)
        throw new ArgumentException("toolPrefab must have an ISkinnableItem component.", nameof(toolPrefab));
    }
    #endif

    public class HandlerInteractWithUnit : PlayerInteractableHandlerBase<ResourceDeposit, Unit>
    {
      public override void Handle(ResourceDeposit deposit, Unit unit)
      {
        unit.SetNewJob(new GatheringJob(unit, deposit));
      }
    }
  }
}
