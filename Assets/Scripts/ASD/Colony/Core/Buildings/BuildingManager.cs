﻿using System.Collections.Generic;
using ASD.Colony.Core.Abstract;
using ASD.Colony.Core.Player.Abstract;
using UnityEngine;

namespace ASD.Colony.Core.Buildings
{
  public class BuildingManager : ManagerBase<Building>
  {
    public static BuildingManager Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<BuildingManager>();
    }

    public BuildingManager() : base(new HashSet<Building>()) { }

    public Building Instantiate<TBuilding>(IPlayer owner, TBuilding prefab)
      where TBuilding : Building =>
      Instantiate(owner, prefab, Vector3.zero, Quaternion.identity);

    public TBuilding Instantiate<TBuilding>(IPlayer owner, TBuilding prefab, Vector3 worldPosition, Quaternion rotation)
      where TBuilding : Building
    {
      GameObject go = Instantiate(prefab.gameObject, worldPosition, rotation);
      TBuilding building = go.GetComponent<TBuilding>();
      building.Owner = owner;
      Register(building);

      return building;
    }
  }
}
