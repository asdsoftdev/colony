﻿using System;
using UnityEngine;

namespace ASD.Colony.Core.Buildings
{
  [RequireComponent(typeof(Collider))]
  public class BuildingPreview : MonoBehaviour
  {
    [SerializeField] private Building buildingPrefab;
    private new Collider collider;
    private int colliderCount;

    public Building BuildingPrefab => buildingPrefab;
    public Collider Collider => collider;

    private void Awake()
    {
      collider = GetComponent<Collider>();
      if (!collider.isTrigger)
        Debug.LogWarning("BuildingPreview collider is not trigger!", this);
    }

    private void OnTriggerEnter(Collider other)
    {
      ++colliderCount;
    }

    private void OnTriggerExit(Collider other)
    {
      --colliderCount;
    }

    public bool IsOnValidPosition() => colliderCount == 0;
  }
}
