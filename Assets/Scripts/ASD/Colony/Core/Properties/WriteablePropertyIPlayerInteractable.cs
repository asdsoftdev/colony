﻿using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Lib.Core.Properties.Abstract;

namespace ASD.Colony.Core.Properties
{
  public class WriteablePropertyIPlayerInteractable : WriteablePropertyBase<IPlayerInteractable> { }
}
