﻿using System;
using System.Collections.Generic;
using ASD.Colony.Core.Player.Interaction.Abstract;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ASD.Colony.Core
{
  public static class Utils
  {
    public static T CreateSingletonInstance<T>()
      where T : Component =>
      (T) CreateSingletonInstance(typeof(T));

    public static Component CreateSingletonInstance(Type t)
    {
      if (!t.IsSubclassOf(typeof(Component)))
        throw new ArgumentException(nameof(t));

      GameObject go = new GameObject(t.Name);
      Component component = go.AddComponent(t);
      Object.DontDestroyOnLoad(component);

      return component;
    }

    public static IEnumerable<T> MarkSurroundingObjects<T>(Vector3 position, float radius, int layerMask = ~0,
      QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
      where T : Component
    {
      // ReSharper disable once Unity.PreferNonAllocApi
      Collider[] colliders = Physics.OverlapSphere(position, radius, layerMask, queryTriggerInteraction);
      foreach (Collider collider in colliders)
      {
        T component = collider.GetComponent<T>();
        if (component)
          yield return component;
      }
    }

    public static bool RaycastFromCameraThroughScreenPoint(UnityEngine.Camera camera, Vector3 screenPoint, out RaycastHit hit,
      float maxDistance = Constants.MAX_CAMERA_RAY_DISTANCE, int layerMask = -1,
      QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
      Ray ray = camera.ScreenPointToRay(screenPoint);

      return Physics.Raycast(ray, out hit, maxDistance, layerMask, queryTriggerInteraction);
    }

    public static bool RaycastFromCameraThroughScreenPoint(Vector3 screenPoint, out RaycastHit hit,
      float maxDistance = Constants.MAX_CAMERA_RAY_DISTANCE, int layerMask = -1,
      QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal) =>
      RaycastFromCameraThroughScreenPoint(UnityEngine.Camera.main, screenPoint, out hit, maxDistance, layerMask, queryTriggerInteraction);

    public static bool RaycastFromCameraThroughMousePosition(out RaycastHit hit, float maxDistance = Constants.MAX_CAMERA_RAY_DISTANCE,
      int layerMask = -1, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal) =>
      RaycastFromCameraThroughMousePosition(UnityEngine.Camera.main, out hit, maxDistance, layerMask, queryTriggerInteraction);

    public static bool RaycastFromCameraThroughMousePosition(UnityEngine.Camera camera, out RaycastHit hit,
      float maxDistance = Constants.MAX_CAMERA_RAY_DISTANCE,
      int layerMask = -1, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
      => RaycastFromCameraThroughScreenPoint(camera, Input.mousePosition, out hit, maxDistance, layerMask, queryTriggerInteraction);

    [CanBeNull]
    public static T GetComponentUnderMouse<T>(float maxDistance = Constants.MAX_CAMERA_RAY_DISTANCE,
      int layerMask = -1, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
      where T : class
      => GetComponentUnderMouse<T>(UnityEngine.Camera.main, maxDistance, layerMask, queryTriggerInteraction);

    [CanBeNull]
    public static T GetComponentUnderMouse<T>(UnityEngine.Camera camera, float maxDistance = Constants.MAX_CAMERA_RAY_DISTANCE,
      int layerMask = -1, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
      where T : class
    {
      if (!RaycastFromCameraThroughMousePosition(camera, out RaycastHit hit, maxDistance, layerMask, QueryTriggerInteraction.Collide))
        return null;

      if (!hit.collider)
        return null;

      GameObject go = hit.collider.gameObject;
      T component = go.GetComponent<T>();

      return component;
    }
  }
}
