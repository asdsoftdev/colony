﻿using System.Collections;
using UnityEngine;

namespace ASD.Colony.Core
{
  public class Scheduler : MonoBehaviour
  {
    public static Scheduler Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<Scheduler>();
    }

    public Coroutine StartCoroutineSingle(IEnumerator coroutine, CancellationToken token) =>
      StartCoroutine(ExecCoroutineCoroutine(coroutine, token));

    public Coroutine ChainCoroutines(CancellationToken token, params IEnumerator[] coroutines) =>
      StartCoroutineSingle(ChainCoroutinesCoroutine(token, coroutines), token);

    public Coroutine ChainCoroutines(params IEnumerator[] coroutines) =>
      ChainCoroutines(new CancellationToken(), coroutines);

    private static IEnumerator ExecCoroutineCoroutine(IEnumerator coroutine, CancellationToken token)
    {
      while (!token.IsCancellationRequested && coroutine.MoveNext())
        yield return coroutine.Current;
    }

    private IEnumerator ChainCoroutinesCoroutine(CancellationToken token, params IEnumerator[] coroutines)
    {
      // ReSharper disable once LoopCanBeConvertedToQuery
      foreach (IEnumerator coroutine in coroutines)
      {
        if (token.IsCancellationRequested)
          yield break;

        yield return StartCoroutineSingle(coroutine, token);
      }
    }

    public class CancellationToken
    {
      public bool IsCancellationRequested { get; private set; }

      public void RequestCancellation()
      {
        IsCancellationRequested = true;
      }
    }
  }
}
