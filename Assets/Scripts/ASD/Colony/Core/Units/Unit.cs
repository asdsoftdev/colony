﻿using System;
using System.Collections.Generic;
using System.Linq;
using ASD.Colony.Core.Buildings;
using ASD.Colony.Core.Character;
using ASD.Colony.Core.Character.Stats;
using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Colony.Core.Skinning;
using ASD.Colony.Core.Skinning.Abstract;
using ASD.Colony.Core.Teams;
using ASD.Colony.Core.Units.Jobs.Abstract;
using ASD.Colony.Core.Units.Jobs.Impl;
using ASD.Lib.Core.Character.Health;
using JetBrains.Annotations;
using UnityEngine;

namespace ASD.Colony.Core.Units
{
  [RequireComponent(typeof(CharacterController3), typeof(StatsController), typeof(HealthController))]
  public class Unit : PlayerInteractableBase, ISkinnable
  {
    [SerializeField] private List<SkinnableSlotMapping> skinnableSlotMappingsList;
    private IReadOnlyDictionary<SkinnableSlot, Transform> skinnableSlotMapping;
    private readonly IDictionary<SkinnableSlot, GameObject> skinnables = new Dictionary<SkinnableSlot, GameObject>();

    [CanBeNull]
    public IUnitJob Job { get; private set; }

    public Animator Animator { get; private set; }
    public CharacterController3 Character { get; private set; }
    public StatsController StatsController { get; private set; }
    public HealthController HealthController { get; private set; }

    private void Awake()
    {
      skinnableSlotMapping = skinnableSlotMappingsList.ToDictionary(m => m.Slot, m => m.Transform);
      Character = GetComponent<CharacterController3>();
      Animator = GetComponentInChildren<Animator>();
      StatsController = GetComponent<StatsController>();
      HealthController = GetComponent<HealthController>();
    }

    private void OnEnable()
    {
      UnitManager.Instance.Register(this);
      Job?.StartJob();
    }

    private void OnDisable()
    {
      Job?.StopJob();
      UnitManager.Instance.Unregister(this);
    }

    public void SetNewJob([CanBeNull] IUnitJob job)
    {
      Job?.StopJob();
      Job = job;
      job?.StartJob();
    }

    public void Equip(SkinnableSlot slot, ISkinnableItem item)
    {
      if (!skinnableSlotMapping.TryGetValue(slot, out Transform t))
        throw new InvalidOperationException($"Can't equip in slot {slot}");

      Unequip(slot);

      GameObject prefab = ((Component) item).gameObject;
      GameObject instance = Instantiate(prefab, t.position, t.rotation, t);

      skinnables[slot] = instance;
    }

    public void Unequip(SkinnableSlot slot)
    {
      if (skinnables.TryGetValue(slot, out GameObject skinnable))
        Destroy(skinnable);
    }

    public class HandlerInteractWithResourceDeposit : PlayerInteractableHandlerBase<Unit, ResourceDeposit>
    {
      public override void Handle(Unit unit, ResourceDeposit deposit)
      {
        unit.SetNewJob(new GatheringJob(unit, deposit));
      }
    }

    public class HandlerInteractWithUnit : PlayerInteractableHandlerBase<Unit, Unit>
    {
      public override void Handle(Unit unit, Unit target)
      {
        if (TeamManager.Instance.AreEnemies(unit, target))
          unit.SetNewJob(new AttackEnemyUnitJob(unit, target));
      }
    }
  }
}
