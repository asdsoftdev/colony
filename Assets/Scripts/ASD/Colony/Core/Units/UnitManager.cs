﻿using System.Collections.Generic;
using System.Linq;
using ASD.Colony.Core.Abstract;
using ASD.Lib.Core;
using UnityEngine;

namespace ASD.Colony.Core.Units
{
  public class UnitManager : ManagerBase<Unit>
  {
    public UnitManager() : base(new HashSet<Unit>(new UnityEngineObjectComparer())) { }

    public IEnumerable<Unit> IdleUnits => RegisteredObjects.Where(unit => unit.Job == null);

    public static UnitManager Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<UnitManager>();
    }
  }
}
