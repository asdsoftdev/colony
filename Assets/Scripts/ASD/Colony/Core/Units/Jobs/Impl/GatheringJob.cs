﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ASD.Colony.Core.Buildings;
using ASD.Colony.Core.Extensions;
using ASD.Colony.Core.Resources;
using ASD.Colony.Core.Skinning;
using ASD.Colony.Core.Skinning.Abstract;
using ASD.Colony.Core.Units.Jobs.Abstract;
using JetBrains.Annotations;
using UnityEngine;
using static MoreLinq.Extensions.MinByExtension;

namespace ASD.Colony.Core.Units.Jobs.Impl
{
  public class GatheringJob : UnitJob
  {
    private const float RESOURCE_NODE_RADIUS = 50f;

    /// <summary>
    ///   Maps the Resource to parameter hash for the animator bool that represents the action assigned to the resource.
    /// </summary>
    private static IReadOnlyDictionary<Resource, int> ResourceToAnimatorParameterHashMapping { get; } = new Dictionary<Resource, int>
    {
      {Resource.Wood, Animator.StringToHash("isChoppingTree")}, {Resource.Stone, Animator.StringToHash("isMining")}
    };

    private NodeGatheringJob innerJob;

    public ResourceDeposit ResourceDeposit { get; }

    public GatheringJob(Unit unit, ResourceDeposit deposit) : base(unit) => ResourceDeposit = deposit;

    protected override IEnumerator JobCoroutine()
    {
      yield return MoveToDepositAndDressUp().StartAsCoroutine(CancellationToken);

      while (!CancellationToken.IsCancellationRequested)
      {
        ResourceNode node = GetSuitableResourceNode();
        if (node is null)
          yield break;

        yield return MarkAndMoveToNode(node).ChainAsCoroutine(CancellationToken,
          StartGathering(),
          StopGathering(),
          MoveToClosestDropOff(),
          DepositResources());
      }
    }

    public override void StopJob()
    {
      base.StopJob();
      StopInnerJob();
    }

    [CanBeNull]
    private ResourceNode GetSuitableResourceNode()
    {
      ResourceNode suitableNode = Utils
        .MarkSurroundingObjects<ResourceNode>(ResourceDeposit.transform.position, RESOURCE_NODE_RADIUS)
        .Where(node => node.Resource == ResourceDeposit.Resource)
        .Where(node => node.State.IsBusy.Value == false)
        .MinBy(node => Vector3.Distance(node.transform.position, Unit.transform.position))
        .FirstOrDefault();

      return suitableNode;
    }

    private IEnumerator MoveToDepositAndDressUp()
    {
      Vector3 position = ResourceDeposit.transform.position;

      yield return Unit.Character.MoveTo(position).ChainAsCoroutine(CancellationToken,
        Unit.Character.LookAt(position));

      IReadOnlyDictionary<SkinnableSlot, ISkinnableItem> items = ResourceDeposit.SupplyFor(Unit);
      foreach (KeyValuePair<SkinnableSlot, ISkinnableItem> pair in items)
      {
        SkinnableSlot slot = pair.Key;
        ISkinnableItem item = pair.Value;
        Unit.Equip(slot, item);
      }
    }

    private IEnumerator MoveToClosestDropOff()
    {
      Vector3 position = ResourceDeposit.transform.position;

      yield return Unit.Character.MoveTo(position).ChainAsCoroutine(CancellationToken,
        Unit.Character.LookAt(position));
    }

    private IEnumerator DepositResources()
    {
      ResourceManager.Instance.Increase(innerJob.ResourceNode.Resource, innerJob.CurrentResource);
      yield break;
    }

    private IEnumerator MarkAndMoveToNode(ResourceNode node)
    {
      innerJob = new NodeGatheringJob(node);
      node.State.GatheringUnit = Unit;

      Vector3 position = node.transform.position;
      yield return Unit.Character.MoveTo(position).ChainAsCoroutine(CancellationToken,
        Unit.Character.LookAt(position));
    }

    private IEnumerator StartGathering()
    {
      Unit.Animator.SetBool(ResourceToAnimatorParameterHashMapping[innerJob.ResourceNode.Resource], true);

      yield return innerJob.StartJob();
    }

    private IEnumerator StopGathering()
    {
      StopInnerJob();
      yield return Unit.Character.Stop().StartAsCoroutine();
    }

    private void StopInnerJob()
    {
      if (innerJob is null)
        return;

      innerJob.StopJob();
      Unit.Animator.SetBool(ResourceToAnimatorParameterHashMapping[innerJob.ResourceNode.Resource], false);
    }

    private class NodeGatheringJob : UnitJobBase
    {
      public float CurrentResource { get; private set; }
      public float MaxResource { get; } = 20;
      public ResourceNode ResourceNode { get; }

      public NodeGatheringJob(ResourceNode resourceNode) => ResourceNode = resourceNode;

      protected override IEnumerator JobCoroutine()
      {
        while (!CancellationToken.IsCancellationRequested && CurrentResource < MaxResource)
        {
          CurrentResource = Mathf.Clamp(CurrentResource + ResourceNode.Yield * Time.deltaTime, 0, MaxResource);
          yield return null;
        }
      }

      public override void StopJob()
      {
        base.StopJob();
        ResourceNode.State.GatheringUnit = null;
      }
    }
  }
}
