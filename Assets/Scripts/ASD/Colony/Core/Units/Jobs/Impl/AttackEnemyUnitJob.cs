﻿using System;
using System.Collections;
using ASD.Colony.Core.Character.Stats.Impl;
using ASD.Colony.Core.Extensions;
using ASD.Colony.Core.Teams;
using ASD.Colony.Core.Units.Jobs.Abstract;
using UnityEngine;

namespace ASD.Colony.Core.Units.Jobs.Impl
{
  public class AttackEnemyUnitJob : UnitJob
  {
    private readonly Unit target;

    public AttackEnemyUnitJob(Unit unit, Unit target) : base(unit)
    {
      if (!TeamManager.Instance.AreEnemies(unit.Owner, target.Owner))
        throw new ArgumentException($"{nameof(unit)} and {nameof(target)} must be enemies.");

      this.target = target;
    }

    protected override IEnumerator JobCoroutine()
    {
      Scheduler.CancellationToken moveToken = new Scheduler.CancellationToken();
      while (!CancellationToken.IsCancellationRequested)
      {
        float range = Unit.StatsController.Get<RangeStat>().Value;
        float distance = Vector3.Distance(Unit.transform.position, target.transform.position);
        if (distance > range)
        {
          Unit.Character.MoveTo(target.transform.position).StartAsCoroutine(moveToken);
          yield return new WaitForFixedUpdate();

          continue;
        }

        moveToken.RequestCancellation();
        break;
      }

      yield return Unit.Character.Stop().StartAsCoroutine(CancellationToken);

      while (!CancellationToken.IsCancellationRequested)
      {
        float damage = Unit.StatsController.Get<AttackStat>().Value;
        float attackSpeed = Unit.StatsController.Get<AttackSpeedStat>().Value;

        Unit.HealthController.TakeDamage(damage, Unit);
        yield return new WaitForSeconds(attackSpeed);
      }
    }
  }
}
