﻿using System.Collections;
using ASD.Colony.Core.Extensions;
using UnityEngine;

namespace ASD.Colony.Core.Units.Jobs.Abstract
{
  public abstract class UnitJobBase : IUnitJob
  {
    protected readonly Scheduler.CancellationToken CancellationToken = new Scheduler.CancellationToken();
    public Coroutine Coroutine { get; private set; }

    public virtual Coroutine StartJob()
    {
      Coroutine = JobCoroutine().StartAsCoroutine(CancellationToken);
      return Coroutine;
    }

    public virtual void StopJob()
    {
      CancellationToken.RequestCancellation();
    }

    protected abstract IEnumerator JobCoroutine();
  }
}
