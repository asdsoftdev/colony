﻿using UnityEngine;

namespace ASD.Colony.Core.Units.Jobs.Abstract
{
  public interface IUnitJob
  {
    Coroutine Coroutine { get; }

    /// <summary>
    /// Starts the job as a coroutine and returns a coroutine that waits for the job's completion.
    /// </summary>
    /// <returns></returns>
    Coroutine StartJob();

    void StopJob();
  }
}
