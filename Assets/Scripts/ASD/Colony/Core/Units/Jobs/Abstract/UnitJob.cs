﻿namespace ASD.Colony.Core.Units.Jobs.Abstract
{
  public abstract class UnitJob : UnitJobBase
  {
    public Unit Unit { get; }

    public UnitJob(Unit unit) => Unit = unit;
  }
}
