﻿namespace ASD.Colony.Input.Abstract
{
  public interface IPlayerInputHandler
  {
    bool IsHandlingInput { get; }

    void PauseInputHandling();

    bool ResumeInputHandling();
  }
}
