﻿using System;
using System.Collections;
using UnityEngine;

namespace ASD.Colony.Input.Abstract
{
  public abstract class PlayerInputHandlerBase : MonoBehaviour, IPlayerInputHandler
  {
    private int inputHandlingPausesRequested;

    public bool IsHandlingInput => inputHandlingPausesRequested == 0;

    protected virtual void Start()
    {
      PlayerInputHandlerManager.Instance.Register(this);
      PauseInputHandling();
    }

    public void PauseInputHandling()
    {
      ++inputHandlingPausesRequested;
      if (inputHandlingPausesRequested == 1)
        OnPause();
    }

    public bool ResumeInputHandling()
    {
      inputHandlingPausesRequested = Mathf.Clamp(inputHandlingPausesRequested - 1, 0, int.MaxValue);

      if (inputHandlingPausesRequested == 0)
        OnResume();

      return IsHandlingInput;
    }

    protected virtual void OnPause() { }

    protected virtual void OnResume() { }

    protected IEnumerator MakeInfiniteCoroutine(Action action)
    {
      while (true)
      {
        if (IsHandlingInput)
          action();
        yield return null;
      }
    }
  }
}
