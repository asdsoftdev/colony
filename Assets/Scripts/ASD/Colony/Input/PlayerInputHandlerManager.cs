﻿using System;
using System.Collections.Generic;
using System.Linq;
using ASD.Colony.Core;
using ASD.Colony.Core.Abstract;
using ASD.Colony.Input.Abstract;
using ASD.Lib.Core.Collections;
using UnityEngine;

namespace ASD.Colony.Input
{
  [DefaultExecutionOrder(-10000)]
  public class PlayerInputHandlerManager : ManagerBase<IPlayerInputHandler>
  {
    private readonly UniqueStack<IPlayerInputHandler> handlerStack = new UniqueStack<IPlayerInputHandler>();

    public IPlayerInputHandler CurrentHandler => handlerStack.Peek();

    public PlayerInputHandlerManager() : base(new HashSet<IPlayerInputHandler>()) { }

    public static PlayerInputHandlerManager Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<PlayerInputHandlerManager>();
    }

    public void Push(IPlayerInputHandler handler)
    {
      if (!RegisteredObjects.Contains(handler))
        throw new ArgumentException(nameof(handler));

      if (handlerStack.Any())
        CurrentHandler.PauseInputHandling();

      handler.ResumeInputHandling();
      handlerStack.Push(handler);
    }

    public IPlayerInputHandler Pop()
    {
      IPlayerInputHandler handler = handlerStack.Pop();
      handler.PauseInputHandling();

      CurrentHandler.ResumeInputHandling();

      return handler;
    }
  }
}
