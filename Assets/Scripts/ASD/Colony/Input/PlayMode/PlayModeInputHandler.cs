﻿using System;
using System.Linq;
using ASD.Colony.Core;
using ASD.Colony.Core.Player;
using ASD.Colony.Core.Player.Interaction;
using ASD.Colony.Core.Player.Interaction.Abstract;
using ASD.Colony.Input.Abstract;
using ASD.Colony.UI;
using ASD.Colony.UI.PlayMode;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ASD.Colony.Input.PlayMode
{
  public class PlayModeInputHandler : PlayerInputHandlerBase, ICancelHandler, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
  {
    public static PlayModeInputHandler Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<PlayModeInputHandler>();
    }

    private int selectableLayerMask;
    private int terrainLayerMask;

    private void Awake()
    {
      selectableLayerMask = LayerMask.GetMask("PlayerSelectable");
      terrainLayerMask = LayerMask.GetMask("Terrain", "Water");

      GameObject prefab = Resources.Load<GameObject>("Prefabs/UI/PlayMode/Input/PlayModeInputHandlerCanvas");
      GameObject canvas = Instantiate(prefab, transform);
      selectionRectangle = canvas.GetComponentInChildren<SelectionRectangleUI>();
    }

    protected override void Start()
    {
      base.Start();
      PlayerInputHandlerManager.Instance.Push(this);
    }

    protected override void OnPause()
    {
      base.OnPause();
      PlayModeCanvas.Instance.gameObject.SetActive(false);
      gameObject.SetActive(false);
    }

    protected override void OnResume()
    {
      base.OnResume();
      PlayModeCanvas.Instance.gameObject.SetActive(true);
      gameObject.SetActive(true);
    }

    public void OnCancel(BaseEventData eventData)
    {
      if (!IsHandlingInput)
        return;

      PlayerInteractionManager.Instance.Selection.DeselectAll();
    }

#region CLICK_HANDLING

    private void SelectObjectUnderMouse()
    {
      PlayerInteractionManager.Instance.Selection.DeselectAll();

      IPlayerInteractable itemUnderMouse = Utils.GetComponentUnderMouse<IPlayerInteractable>(layerMask: selectableLayerMask);
      if (itemUnderMouse is null)
        return;

      PlayerInteractionManager.Instance.Selection.Select(itemUnderMouse);
    }

    private void InteractWithObjectUnderMouse()
    {
      IPlayerInteractable itemUnderMouse = Utils.GetComponentUnderMouse<IPlayerInteractable>(layerMask: selectableLayerMask);
      if (itemUnderMouse is null)
        return;

      foreach (IPlayerInteractable subject in PlayerInteractionManager.Instance.Selection.SelectedItems)
      {
        if (ReferenceEquals(subject.Owner, HumanPlayer.Instance))
          PlayerInteractionManager.Instance.Handle(subject, itemUnderMouse);
      }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
      if (!IsHandlingInput)
        return;

      switch (eventData.button)
      {
        case PointerEventData.InputButton.Left:
          if (!eventData.dragging)
            SelectObjectUnderMouse();
          return;

        case PointerEventData.InputButton.Right:
          if (!eventData.dragging)
            InteractWithObjectUnderMouse();
          return;

        case PointerEventData.InputButton.Middle:
          return;

        default:
          throw new ArgumentOutOfRangeException();
      }
    }

#endregion

#region DRAG_HANDLING

    private SelectionRectangleUI selectionRectangle;
    private Vector3 screenPositionStart;
    private Vector3 screenPositionEnd;

    private float rectX, rectY, rectWidth, rectHeight;

    public void OnBeginDrag(PointerEventData eventData)
    {
      if (!IsHandlingInput || eventData.button != PointerEventData.InputButton.Left)
        return;

      screenPositionStart = eventData.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
      if (!IsHandlingInput || eventData.button != PointerEventData.InputButton.Left)
        return;

      screenPositionEnd = eventData.position;

      rectX = Mathf.Min(screenPositionStart.x, screenPositionEnd.x);
      rectY = Mathf.Min(screenPositionStart.y, screenPositionEnd.y);

      rectWidth = Mathf.Abs(screenPositionStart.x - screenPositionEnd.x);
      rectHeight = Mathf.Abs(screenPositionStart.y - screenPositionEnd.y);

      selectionRectangle.Show();
      selectionRectangle.SetRect(new Rect(rectX, rectY, rectWidth, rectHeight));
    }

    private static readonly Vector3 BoxHalfExtentsOffset = Vector3.up * 0.2f;

    public void OnEndDrag(PointerEventData eventData)
    {
      if (!IsHandlingInput || eventData.button != PointerEventData.InputButton.Left)
        return;

      selectionRectangle.Hide();

      screenPositionEnd = eventData.position;

      Camera cam = Camera.main;
      Vector2 bottomLeft = new Vector2(rectX, rectY);
      Vector2 bottomRight = new Vector2(rectX + rectWidth, rectY);
      Vector2 topRight = new Vector2(rectX + rectWidth, rectY + rectHeight);
      Vector2 topLeft = new Vector2(rectX, rectY + rectHeight);

      if (!Utils.RaycastFromCameraThroughScreenPoint(cam, bottomLeft, out RaycastHit blHit, layerMask: terrainLayerMask) ||
          !Utils.RaycastFromCameraThroughScreenPoint(cam, bottomRight, out RaycastHit brHit, layerMask: terrainLayerMask) ||
          !Utils.RaycastFromCameraThroughScreenPoint(cam, topRight, out RaycastHit trHit, layerMask: terrainLayerMask) ||
          !Utils.RaycastFromCameraThroughScreenPoint(cam, topLeft, out RaycastHit tlHit, layerMask: terrainLayerMask))
        return;

      RaycastHit[] hits = {blHit, brHit, trHit, tlHit};
      float minX = hits.Min(hit => hit.point.x);
      float minY = hits.Min(hit => hit.point.y);
      float minZ = hits.Min(hit => hit.point.z);
      float maxX = hits.Max(hit => hit.point.x);
      float maxY = hits.Max(hit => hit.point.y);
      float maxZ = hits.Max(hit => hit.point.z);

      Vector3 center = new Vector3(minX + maxX, minY + maxY, minZ + maxZ) / 2f;
      Vector3 halfExtents = new Vector3(maxX - minX, maxY - minY, maxZ - minZ) / 2f + BoxHalfExtentsOffset;

      PlayerInteractionManager.Instance.Selection.DeselectAll();

      // ReSharper disable once Unity.PreferNonAllocApi
      Collider[] colliders = Physics.OverlapBox(center, halfExtents, Quaternion.identity, selectableLayerMask, QueryTriggerInteraction.UseGlobal);
      foreach (Collider coll in colliders)
      {
        GameObject go = coll.gameObject;
        IPlayerInteractable interactable = go.GetComponent<IPlayerInteractable>();
        PlayerInteractionManager.Instance.Selection.Select(interactable);
      }
    }

#endregion
  }
}
