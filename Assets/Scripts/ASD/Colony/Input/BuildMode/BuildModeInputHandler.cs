﻿using System;
using ASD.Colony.Core;
using ASD.Colony.Core.Buildings;
using ASD.Colony.Core.Extensions;
using ASD.Colony.Core.Player;
using ASD.Colony.Input.Abstract;
using ASD.Colony.UI.BuildMode;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ASD.Colony.Input.BuildMode
{
  public class BuildModeInputHandler : PlayerInputHandlerBase, IPointerClickHandler
  {
    public static BuildModeInputHandler Instance { get; private set; }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
      Instance = Utils.CreateSingletonInstance<BuildModeInputHandler>();
    }

    public BuildingPreview BuildingPreviewPrefab
    {
      get => buildingPreviewPrefab;
      set
      {
        if (buildingPreviewInstance)
          Destroy(buildingPreviewInstance.gameObject);

        buildingPreviewPrefab = value;
        if (!buildingPreviewPrefab)
          return;

        GameObject go = Instantiate(buildingPreviewPrefab.gameObject);
        buildingPreviewInstance = go.GetComponent<BuildingPreview>();
      }
    }

    private BuildingPreview buildingPreviewPrefab;
    private BuildingPreview buildingPreviewInstance;

    private new Camera camera;
    private int terrainLayerMask;

    private void Awake()
    {
      terrainLayerMask = LayerMask.GetMask("Terrain");

      GameObject canvas = Resources.Load<GameObject>("Prefabs/UI/BuildMode/Input/BuildModeInputHandlerCanvas");
      Instantiate(canvas, transform);
    }

    protected override void Start()
    {
      base.Start();
      camera = Camera.main;
      MakeInfiniteCoroutine(BuildingPreviewTrackPointer).StartAsCoroutine();
    }

    protected override void OnPause()
    {
      base.OnPause();
      BuildModeCanvas.Instance.gameObject.SetActive(false);
      gameObject.SetActive(false);
    }

    protected override void OnResume()
    {
      base.OnResume();
      BuildModeCanvas.Instance.gameObject.SetActive(true);
      gameObject.SetActive(true);
    }

    public void EnterBuildMode()
    {
      if (IsHandlingInput)
        return;

      PlayerInputHandlerManager.Instance.Push(this);
    }

    public void ExitBuildMode()
    {
      if (!IsHandlingInput)
        return;

      if (buildingPreviewInstance)
        Destroy(buildingPreviewInstance.gameObject);

      BuildingPreviewPrefab = null;
      buildingPreviewInstance = null;

      PlayerInputHandlerManager.Instance.Pop();
    }

    private void BuildingPreviewTrackPointer()
    {
      if (!buildingPreviewInstance)
        return;

      if (!Utils.RaycastFromCameraThroughMousePosition(camera, out RaycastHit hit,
        layerMask: terrainLayerMask, queryTriggerInteraction: QueryTriggerInteraction.Collide))
        return;

      Vector3 worldPosition = hit.point;
      buildingPreviewInstance.transform.position = worldPosition;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
      if (!buildingPreviewInstance)
        return;

      if (eventData.used)
        return;

      eventData.Use();

      if (!buildingPreviewInstance.IsOnValidPosition())
        return;

      Transform preview = buildingPreviewInstance.transform;
      BuildingManager.Instance.Instantiate(HumanPlayer.Instance, BuildingPreviewPrefab.BuildingPrefab, preview.position, preview.rotation);

      ExitBuildMode();
    }
  }
}
