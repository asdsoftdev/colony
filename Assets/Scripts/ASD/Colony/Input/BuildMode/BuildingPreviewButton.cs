﻿using ASD.Colony.Core.Buildings;
using UnityEngine;
using UnityEngine.UI;

namespace ASD.Colony.Input.BuildMode
{
  [RequireComponent(typeof(Button))]
  public class BuildingPreviewButton : MonoBehaviour
  {
    [SerializeField] private BuildingPreview buildingPreviewPrefab;
    private Button button;

    private void Awake()
    {
      button = GetComponent<Button>();
      button.onClick.AddListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
      BuildModeInputHandler.Instance.BuildingPreviewPrefab = buildingPreviewPrefab;
    }
  }
}
