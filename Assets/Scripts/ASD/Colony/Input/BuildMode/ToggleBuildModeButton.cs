﻿using UnityEngine;
using UnityEngine.UI;

namespace ASD.Colony.Input.BuildMode
{
  [RequireComponent(typeof(Button))]
  public class ToggleBuildModeButton : MonoBehaviour
  {
    private Button button;

    private void Awake()
    {
      button = GetComponent<Button>();
      button.onClick.AddListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
      if (!BuildModeInputHandler.Instance.IsHandlingInput)
        BuildModeInputHandler.Instance.EnterBuildMode();
      else
        BuildModeInputHandler.Instance.ExitBuildMode();
    }
  }
}
