﻿using ASD.Colony.Core.Player;
using ASD.Colony.Core.Player.Interaction.Abstract;
using UnityEngine;

namespace ASD.Colony.Editor.Debug
{
  [RequireComponent(typeof(IPlayerOwnable))]
  public class DebugSetOwnerToHumanPlayer : MonoBehaviour
  {
    private IPlayerOwnable playerOwnable;

    private void Awake()
    {
      playerOwnable = GetComponent<IPlayerOwnable>();
    }

    private void Start()
    {
      playerOwnable.Owner = HumanPlayer.Instance;
    }
  }
}
