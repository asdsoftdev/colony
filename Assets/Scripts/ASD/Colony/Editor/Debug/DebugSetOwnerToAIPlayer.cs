﻿using ASD.Colony.Core.Player.AI;
using ASD.Colony.Core.Player.Interaction.Abstract;
using UnityEngine;

namespace ASD.Colony.Editor.Debug
{
  [RequireComponent(typeof(IPlayerOwnable))]
  public class DebugSetOwnerToAIPlayer : MonoBehaviour
  {
    [SerializeField] private AIPlayer aiPlayer;
    private IPlayerOwnable playerOwnable;

    private void Awake()
    {
      playerOwnable = GetComponent<IPlayerOwnable>();
    }

    private void Start()
    {
      playerOwnable.Owner = aiPlayer;
    }
  }
}
