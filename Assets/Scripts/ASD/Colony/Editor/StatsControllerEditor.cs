﻿using System;
using System.Collections.Generic;
using ASD.Colony.Core.Character.Stats;
using ASD.Colony.Core.Character.Stats.Abstract;
using ASD.Lib.Editor;
using UnityEditor;

namespace ASD.Colony.Editor
{
  [CustomEditor(typeof(StatsController))]
  public class StatsControllerEditor : CustomEditor<StatsController>
  {
    protected override void LayoutDebugInfo()
    {
      foreach (KeyValuePair<Type, object> pair in Target.GetAll())
      {
        Type type = pair.Key;
        object stat = pair.Value;
        object value = type.GetProperty(nameof(IStat<object>.Value))?.GetValue(stat);

        EditorGUILayout.LabelField(type.Name, value?.ToString());
      }
    }
  }
}
